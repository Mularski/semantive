TEST_HTML = """<html lang="en">
<head>
  <meta charset="utf-8">

  <title>Some title</title>
  <meta name="description" content="The HTML5 Herald">
  <meta name="author" content="SitePoint">

  <link rel="stylesheet" href="#">

</head>

<body>
    <div>
        <h1>This is a simple hello world website</h1>
        <ul>
            <li>first element</li>
            <li>second element</li>
            <li>third element</li>
        </ul>
        <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit.
        </p>
        <img src="/some-url.jpg"> 
        <span class="some-class">
            Some span with some class.
        </span>
    </div>
</body>
</html>"""
