from unittest import mock, TestCase
from unittest.mock import Mock

from bs4 import BeautifulSoup

from src.utils import parse_website
from tests import consts


class ParserTestCase(TestCase):

    def setUp(self) -> None:
        self.patcher = mock.patch('requests.get')
        self.r_mock = self.patcher.start()
        self._create_successful_request_mock()

    def _create_successful_request_mock(self):
        self.responsemock = Mock()
        self.r_mock.return_value = self.responsemock
        self.responsemock.status_code = 200
        self.responsemock.text = consts.TEST_HTML

    def test_parse_website__get_images(self):
        soup = BeautifulSoup(self.responsemock.text)
        img_tag = soup.find('img')
        expected = [img_tag.attrs['src']]
        _, result = parse_website('', parse_images=True)
        self.assertEqual(expected, result)

    def test_parse_website__could_not_load_website(self):
        self.responsemock.ok = False

        url = 'http://some_url.com'
        reg = f"Could not load a website"

        self.assertRaisesRegex(Exception, reg, parse_website, url)
