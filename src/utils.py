import requests
from bs4 import BeautifulSoup


def parse_website(url, parse_text: bool = True, parse_images: bool = False):
    r = requests.get(url)
    if not r.ok:
        raise Exception("Could not load a website.")

    soup = BeautifulSoup(r.text, "html.parser")
    site_text = soup.get_text() if parse_text else None
    site_images = _get_image_urls(soup) if parse_images else []

    return site_text, site_images


def _get_image_urls(soup):
    image_tags = soup.findAll('img')
    image_urls = []
    for image_tag in image_tags:
        image_urls.append(image_tag.get('src'))
    return image_urls


def download_file(url, name):
    img = requests.get(url)
    img_data = img.content
    with open(f'{name}.jpg', 'wb') as handler:
        handler.write(img_data)
