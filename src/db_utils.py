from datetime import datetime

from src.main import session_factory
from src.models import ParsedWebsite, ParsedImage


def create_item(w_info, text, images):
    s = session_factory()
    website = ParsedWebsite()
    website.source_url = w_info.source_url
    website.timestamp = datetime.now()
    website.title = w_info.name
    website.text = text
    s.add(website)
    s.commit()

    for image in images:
        img = ParsedImage()
        img.url = image
        img.website_id = website.id
        s.add(img)

    s.commit()
    s.close()
