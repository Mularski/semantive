import os

import uvicorn
from fastapi import FastAPI
from pydantic import BaseModel
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from src.models import Base, ParsedWebsite, ParsedImage
from src.utils import parse_website, download_file

ErrorTag = 'error'
MessageTag = 'msg'
DownloadsDirectory = 'downloads'


class WebsiteInfo(BaseModel):
    name: str = ''
    source_url: str = ''
    parse_text: bool = True
    parse_images: bool = False


DATABASE_URI = 'postgresql:///semantive'
engine = create_engine(DATABASE_URI)
Base.metadata.create_all(engine)
session_factory = sessionmaker(bind=engine)
app = FastAPI()


@app.get("/")
def read_root():
    return {"msg": "In order to try this API out you could either go to /docs/ page or use CURL. First option is "
                   "prefered. Select one of the possible request method/address combinations, click \"Try it out\""
                   "and then create your request and pass with \"Execute\" button"}


@app.get("/imgs")
async def get_images_by_website_id(website_id: int):
    s = session_factory()
    img_q = s.query(ParsedImage)
    website_q = s.query(ParsedWebsite)
    website = website_q.get(website_id)
    imgs = img_q.filter(ParsedImage.website_id == website_id)
    if not imgs:
        return {ErrorTag: 'Did not find any images for that Website ID'}

    if not os.path.exists(DownloadsDirectory):
        os.makedirs(DownloadsDirectory)

    for image in imgs:
        download_file(website.source_url + image.url, image.id)
    return {MessageTag: 'Successfully downloaded all images.'}


@app.get("/text")
async def get_text_by_website_id(website_id: int, file_name: str = 'strapped.txt'):
    s = session_factory()
    website_q = s.query(ParsedWebsite)
    website = website_q.get(website_id)
    if not website:
        return {ErrorTag: 'Successfully downloaded the strapped text.'}

    if not os.path.exists(DownloadsDirectory):
        os.makedirs(DownloadsDirectory)

    path = os.path.join(DownloadsDirectory, file_name)
    with open(path, 'w') as f:
        f.write(website.text)
    return {MessageTag: 'Successfully downloaded the strapped text.'}


@app.get("/items")
async def get_all_items():
    s = session_factory()
    q = s.query(ParsedWebsite)
    imgs = s.query(ParsedImage)
    resp = {}
    for w in q.all():
        rel_imgs = imgs.filter(ParsedImage.website_id == w.id)
        resp[w.id] = {'title': w.title, 'url': w.source_url, 'text': w.text, 'timestamp': w.timestamp,
                      'images_urls': [img.url for img in rel_imgs]}
    return resp


@app.post("/items")
async def create_entry(w_info: WebsiteInfo):
    if not w_info.parse_images and not w_info.parse_text:
        return {ErrorTag: 'You should set either parse_text or parse_images or both to true'}

    text, images = parse_website(w_info.source_url,
                                 parse_text=w_info.parse_text,
                                 parse_images=w_info.parse_images)
    from src.db_utils import create_item
    create_item(w_info, text, images)
    return {MessageTag: 'Successfully strapped the website.'}


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)
