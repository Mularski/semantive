from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, DateTime, Text, ForeignKey

Base = declarative_base()


class ParsedWebsite(Base):
    __tablename__ = 'parsed_websites'
    id = Column(Integer, primary_key=True, autoincrement=True)
    title = Column(String)
    source_url = Column(String, nullable=False)
    timestamp = Column(DateTime, nullable=False)
    text = Column(Text)


class ParsedImage(Base):
    __tablename__ = 'parsed_images'
    id = Column(Integer, primary_key=True)
    url = Column(String, nullable=False)
    website_id = Column(Integer, ForeignKey('parsed_websites.id'))
